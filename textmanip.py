def count_men(infile):

    with open(infile, 'r') as f:
        content = f.read()

    c1 = content.count(' men ')
    c2 = content.count(' men.')
    count = c1 + c2
    return count


def cap_multi_vowel_words(infile):
    import re
    p = re.compile('\\b\\w*[aeiou][aeiou]\\w*\\b')

    out = open('example_text_capped_vowel_words.txt', 'wt')

    with open(infile, 'r') as f:
        content = f.readlines()
        for line in content:
            it = re.finditer(p, line)
            indices = [m.span(0) for m in it]
            for i in indices:
                line = line[:i[0]] + line[i[0]:i[1]].upper() + line[i[1]:]
            out.write(line)

    out.close()


if __name__ == "__main__":
    cap_multi_vowel_words("example_text.txt")
